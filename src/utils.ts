export const btoaBuffer = (txt: string) => {
    return new Buffer(txt).toString('base64');
};

export const toString = (str: string) => {
    return str == null ? '' : String(str);
};
export const authBasic = (username: string, password: string) => {
    return 'Basic ' + btoaBuffer(`${toString(username)}:${toString(password)}`);
};
