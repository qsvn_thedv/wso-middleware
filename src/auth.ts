import request from 'request-promise';
import invariant from 'invariant';
import { Request, Response, NextFunction } from 'express';
import { authBasic } from './utils';
// const request = axios.create({
//     timeout: 5000,
//     responseType: 'json'
// });

interface IConfig {
    userInfoUrl: string;
    accessTokenUri: string;
    authzizationUrl?: string;
    clientId: string;
    clientSecret: string;
    callbackUrl?: string;
}

interface IConfigParseToken {
    headerKey?: string;
    reqKey?: string;
}

export interface IWSOResponse extends Request {
    isAuthenticate?: boolean;
    user?: {
        [key: string]: any;
    };

    newToken?: {
        access_token: string;
        refresh_token: string;
        scope: string;
        id_token: string;
        token_type: string;
        expires_in: number;
    };

    [key: string]: any;
}

export default class WSO2 {
    config: IConfig;
    constructor(_config: IConfig) {
        invariant(_config.clientId, 'clientId must be required');
        invariant(_config.clientSecret, 'clientSecret must be required');
        invariant(_config.userInfoUrl, 'userInfoUrl must be required');
        invariant(_config.accessTokenUri, 'accessTokenUri must be required');
        this.config = _config;
    }

    authenticate() {
        return (req: IWSOResponse, res: Response, next: NextFunction): any => {
            if (req.isAuthenticate) {
                return next();
            }
            if (!req.headers || !req.headers.authorization) {
                req.user = {};
                req.isAuthenticate = false;
                return next();
            }
            return request({
                json: true,
                method: 'GET',
                uri: this.config.userInfoUrl,
                headers: {
                    Authorization: req.headers.authorization
                }
            })
                .then(response => {
                    req.user = response;
                    req.isAuthenticate = true;
                    return next();
                })
                .catch(() => {
                    req.user = {};
                    req.isAuthenticate = false;
                    return next();
                });
        };
    }

    authenticated() {
        return (req: IWSOResponse, res: Response, next: NextFunction): any => {
            if (req.isAuthenticate) {
                return next();
            }
            if (!req.headers || !req.headers.authorization) {
                return res.status(401).json({
                    error_description: 'Access token missing',
                    error: 'invalid_request'
                });
            }
            return request({
                json: true,
                method: 'GET',
                uri: this.config.userInfoUrl,
                headers: {
                    Authorization: req.headers.authorization
                }
            })
                .then(response => {
                    req.user = response;
                    req.isAuthenticate = true;
                    return next();
                })
                .catch(error => {
                    const response = error.response;
                    if (response.body) {
                        // The request was made and the server responded with a status code
                        // that falls out of the range of 2xx
                        return res.status(response.statusCode).json(response.body);
                    } else {
                        // Something happened in setting up the request that triggered an Error
                        return res.status(response.statusCode).json({
                            error_description: response.statusMessage,
                            error: 'invalid_request'
                        });
                    }
                });
        };
    }

    refreshToken() {
        return (req: IWSOResponse, res: Response, next: NextFunction): any => {
            if (!req.body || !req.body.refresh_token) {
                return res.status(400).json({
                    error_description: 'refresh_token not found',
                    error: 'bad_request'
                });
            }

            return request({
                json: true,
                method: 'POST',
                uri: this.config.accessTokenUri,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    Authorization: authBasic(this.config.clientId, this.config.clientSecret)
                },
                form: {
                    grant_type: 'refresh_token',
                    refresh_token: req.body.refresh_token
                }
            })
                .then(response => {
                    req.newToken = response;
                    return next();
                })
                .catch(error => {
                    const response = error.response;
                    if (response.body) {
                        // The request was made and the server responded with a status code
                        // that falls out of the range of 2xx
                        return res.status(response.statusCode).json(response.body);
                    } else {
                        // Something happened in setting up the request that triggered an Error
                        return res.status(response.statusCode).json({
                            error_description: response.statusMessage,
                            error: 'invalid_request'
                        });
                    }
                });
        };
    }

    parseToken(opts: IConfigParseToken = {}) {
        const headerKey = opts.headerKey || 'Bearer';
        const reqKey = opts.reqKey || 'token';
        return function(req: IWSOResponse, res: Response, next: NextFunction): any {
            if (req.headers && req.headers.authorization) {
                const parts = req.headers.authorization.split(' ');
                if (parts.length === 2 && parts[0] === headerKey) {
                    req[reqKey] = parts[1];
                }
            }
            next();
        };
    }
}
