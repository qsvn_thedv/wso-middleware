"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.btoaBuffer = function (txt) {
    return new Buffer(txt).toString('base64');
};
exports.toString = function (str) {
    return str == null ? '' : String(str);
};
exports.authBasic = function (username, password) {
    return 'Basic ' + exports.btoaBuffer(exports.toString(username) + ":" + exports.toString(password));
};
