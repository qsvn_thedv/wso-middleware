export declare const btoaBuffer: (txt: string) => string;
export declare const toString: (str: string) => string;
export declare const authBasic: (username: string, password: string) => string;
