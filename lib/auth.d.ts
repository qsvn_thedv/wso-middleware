import { Request, Response, NextFunction } from 'express';
interface IConfig {
    userInfoUrl: string;
    accessTokenUri: string;
    authzizationUrl?: string;
    clientId: string;
    clientSecret: string;
    callbackUrl?: string;
}
interface IConfigParseToken {
    headerKey?: string;
    reqKey?: string;
}
export interface IWSOResponse extends Request {
    isAuthenticate?: boolean;
    user?: {
        [key: string]: any;
    };
    newToken?: {
        access_token: string;
        refresh_token: string;
    };
    [key: string]: any;
}
export default class WSO2 {
    config: IConfig;
    constructor(_config: IConfig);
    authenticate(): (req: IWSOResponse, res: Response, next: NextFunction) => any;
    authenticated(): (req: IWSOResponse, res: Response, next: NextFunction) => any;
    refreshToken(): (req: IWSOResponse, res: Response, next: NextFunction) => any;
    parseToken(opts?: IConfigParseToken): (req: IWSOResponse, res: Response, next: NextFunction) => any;
}
export {};
