"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var request_promise_1 = tslib_1.__importDefault(require("request-promise"));
var invariant_1 = tslib_1.__importDefault(require("invariant"));
var utils_1 = require("./utils");
var WSO2 = (function () {
    function WSO2(_config) {
        invariant_1.default(_config.clientId, 'clientId must be required');
        invariant_1.default(_config.clientSecret, 'clientSecret must be required');
        invariant_1.default(_config.userInfoUrl, 'userInfoUrl must be required');
        invariant_1.default(_config.accessTokenUri, 'accessTokenUri must be required');
        this.config = _config;
    }
    WSO2.prototype.authenticate = function () {
        var _this = this;
        return function (req, res, next) {
            if (req.isAuthenticate) {
                return next();
            }
            if (!req.headers || !req.headers.authorization) {
                req.user = {};
                req.isAuthenticate = false;
                return next();
            }
            return request_promise_1.default({
                json: true,
                method: 'GET',
                uri: _this.config.userInfoUrl,
                headers: {
                    Authorization: req.headers.authorization
                }
            })
                .then(function (response) {
                req.user = response;
                req.isAuthenticate = true;
                return next();
            })
                .catch(function () {
                req.user = {};
                req.isAuthenticate = false;
                return next();
            });
        };
    };
    WSO2.prototype.authenticated = function () {
        var _this = this;
        return function (req, res, next) {
            if (req.isAuthenticate) {
                return next();
            }
            if (!req.headers || !req.headers.authorization) {
                return res.status(401).json({
                    error_description: 'Access token missing',
                    error: 'invalid_request'
                });
            }
            return request_promise_1.default({
                json: true,
                method: 'GET',
                uri: _this.config.userInfoUrl,
                headers: {
                    Authorization: req.headers.authorization
                }
            })
                .then(function (response) {
                req.user = response;
                req.isAuthenticate = true;
                return next();
            })
                .catch(function (error) {
                var response = error.response;
                if (response.body) {
                    return res.status(response.statusCode).json(response.body);
                }
                else {
                    return res.status(response.statusCode).json({
                        error_description: response.statusMessage,
                        error: 'invalid_request'
                    });
                }
            });
        };
    };
    WSO2.prototype.refreshToken = function () {
        var _this = this;
        return function (req, res, next) {
            if (!req.body || !req.body.refresh_token) {
                return res.status(400).json({
                    error_description: 'refresh_token not found',
                    error: 'bad_request'
                });
            }
            return request_promise_1.default({
                json: true,
                method: 'POST',
                uri: _this.config.accessTokenUri,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    Authorization: utils_1.authBasic(_this.config.clientId, _this.config.clientSecret)
                },
                form: {
                    grant_type: 'refresh_token',
                    refresh_token: req.body.refresh_token
                }
            })
                .then(function (response) {
                req.newToken = response;
                return next();
            })
                .catch(function (error) {
                var response = error.response;
                if (response.body) {
                    return res.status(response.statusCode).json(response.body);
                }
                else {
                    return res.status(response.statusCode).json({
                        error_description: response.statusMessage,
                        error: 'invalid_request'
                    });
                }
            });
        };
    };
    WSO2.prototype.parseToken = function (opts) {
        if (opts === void 0) { opts = {}; }
        var headerKey = opts.headerKey || 'Bearer';
        var reqKey = opts.reqKey || 'token';
        return function (req, res, next) {
            if (req.headers && req.headers.authorization) {
                var parts = req.headers.authorization.split(' ');
                if (parts.length === 2 && parts[0] === headerKey) {
                    req[reqKey] = parts[1];
                }
            }
            next();
        };
    };
    return WSO2;
}());
exports.default = WSO2;
